package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Tarification;

public interface tarificationRepository extends JpaRepository<Tarification, Long> {

}
