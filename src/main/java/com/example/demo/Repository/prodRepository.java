package com.example.demo.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.AppUser;
import com.example.demo.Model.Produit;


public interface prodRepository extends JpaRepository<Produit, Long>{
	
	 Produit findProduitByIdProduit(Long prod_id_produit);

}
