package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Visite;

public interface visiteRepository extends JpaRepository<Visite, Long> {

}
