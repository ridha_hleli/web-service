package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Objectif;

public interface objectifRepository extends JpaRepository<Objectif, Integer>{

}
