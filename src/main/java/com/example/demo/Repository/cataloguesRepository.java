package com.example.demo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.Model.AppUser;
import com.example.demo.Model.Catalogues;


public interface cataloguesRepository extends JpaRepository<Catalogues, Long>{
	Catalogues findByIdCatalogue(Long idCatalogue);

	@Query(value="SELECT * FROM Catalogues WHERE user_id_user=:user_id_user", nativeQuery = true )
	Catalogues findCatalogueByIdUser(@Param("user_id_user") Integer user_id_user);

	
}
