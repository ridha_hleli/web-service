package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.ligne_commande;

public interface LigneCmdRepo extends JpaRepository<ligne_commande, Long> {

}
