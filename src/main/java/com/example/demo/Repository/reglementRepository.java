package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Réglement;

public interface reglementRepository extends JpaRepository<Réglement, Integer> {

}
