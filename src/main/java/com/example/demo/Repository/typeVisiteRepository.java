package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.TypeVisite;

public interface typeVisiteRepository extends JpaRepository<TypeVisite, Integer>{

}
