package com.example.demo.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Client;
import com.example.demo.Model.Tache;
import java.util.*;

public interface tacheRepository extends JpaRepository<Tache, Long>{

	List<Tache> findByClientAndDate(Client client, LocalDate date);

}
