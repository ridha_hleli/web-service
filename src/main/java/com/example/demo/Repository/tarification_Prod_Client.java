package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Tarification_Prod_Client;

public interface tarification_Prod_Client extends JpaRepository<Tarification_Prod_Client, Long> {

}
