package com.example.demo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.data.repository.query.Param;

import com.example.demo.Model.AppUser;
import com.example.demo.Model.Catalogues;


public interface appUserRepository extends JpaRepository<AppUser,Integer> {
	 void deleteUserByIdUser(Integer idUser);

	 List<AppUser> findUserByIdUser(Integer idUser);
	 
	 @Query(value="SELECT catalogue_id_catalogue FROM AppUser where idUser=1", nativeQuery = true )
	 public AppUser getcatalogbyuser(@Param("idUser") Integer idUser);

	//Optional<AppUser> findByNom(String username);
	

	boolean existsByUsername(String userName);

	boolean existsByEmail(String email);

	Optional<AppUser> findByUsername(String username);

}
