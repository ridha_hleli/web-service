package com.example.demo.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Model.Commande;

public interface commandeRepository extends JpaRepository<Commande, Long> {
	//@Query()
	 

	void deleteCommandeByIdCommande(Long idCommande);
	Optional<Commande> findCommandeByIdCommande(Long idCommande);
}
