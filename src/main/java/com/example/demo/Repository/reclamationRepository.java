package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Réclamation;

public interface reclamationRepository extends JpaRepository<Réclamation, Integer> {

}
