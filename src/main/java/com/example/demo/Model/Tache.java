package com.example.demo.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Tache {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    private boolean etat;
	
	
	//@JsonFormat(pattern="yyyy-MM-dd ")
	//@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalDate date;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	private LocalDateTime passedAt = LocalDateTime.now();
	
	@ManyToOne
	//@JoinColumn(name = "idUser")
	private AppUser user;
    
   
   @ManyToOne
   //@JoinColumn(name = "idUser")
	private Client client;


public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public LocalDate getDate() {
	return date;
}

public void setDate(LocalDate date) {
	this.date = date;
}

public LocalDateTime getPassedAt() {
	return passedAt;
}

public boolean isEtat() {
	return etat;
}

public void setEtat(boolean etat) {
	this.etat = etat;
}

public void setPassedAt(LocalDateTime passedAt) {
	this.passedAt = passedAt;
}


public AppUser getUser() {
	return user;
}


public void setUser(AppUser user) {
	this.user = user;
}


public Client getClient() {
	return client;
}


public void setClient(Client client) {
	this.client = client;
}



public Tache(Long id, boolean etat, LocalDate date, LocalDateTime passedAt, AppUser user, Client client) {
	super();
	this.id = id;
	this.etat = etat;
	this.date = date;
	this.passedAt = passedAt;
	this.user = user;
	this.client = client;
}

public Tache() {
	super();
	// TODO Auto-generated constructor stub
}
   
   
}
