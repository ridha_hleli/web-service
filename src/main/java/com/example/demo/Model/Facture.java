package com.example.demo.Model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.criteria.CriteriaBuilder.In;
@Entity
public class Facture {
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
     private Integer idFacture;
     private Integer numFacture;
     private Date date;
	
	@ManyToMany
	 @JoinTable(name = "ReglementFacture",
    joinColumns = {
            @JoinColumn(name = "idFacture", referencedColumnName = "idFacture",
                    nullable = false, updatable = true)},
    inverseJoinColumns = {
            @JoinColumn(name = "idReg", referencedColumnName = "idReg",
                    nullable = false, updatable = true)})
	private List<Réglement> regs;
	
	@ManyToMany
	 @JoinTable(name = "LigneFacture",
   joinColumns = {
           @JoinColumn(name = "idFacture", referencedColumnName = "idFacture",
                   nullable = false, updatable = true)},
   inverseJoinColumns = {
           @JoinColumn(name = "idProduit", referencedColumnName = "idProduit",
                   nullable = false, updatable = true)})
	private List<Produit> prods;

	public Integer getIdFacture() {
		return idFacture;
	}

	public void setIdFacture(Integer idFacture) {
		this.idFacture = idFacture;
	}

	public Integer getNumFacture() {
		return numFacture;
	}

	public void setNumFacture(Integer numFacture) {
		this.numFacture = numFacture;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Réglement> getRegs() {
		return regs;
	}

	public void setRegs(List<Réglement> regs) {
		this.regs = regs;
	}

	public List<Produit> getProds() {
		return prods;
	}

	public void setProds(List<Produit> prods) {
		this.prods = prods;
	}

	public Facture(Integer numFacture, Date date, List<Réglement> regs, List<Produit> prods) {
		super();
		this.numFacture = numFacture;
		this.date = date;
		this.regs = regs;
		this.prods = prods;
	}

	public Facture() {
		super();
	}
	
	
	
}
