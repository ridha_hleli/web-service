package com.example.demo.Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
public class Réclamation implements Serializable {
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Integer idReclamation;
	   private String sujet;
	   private String description;
	   @JsonFormat(pattern="yyyy-MM-dd ")
	   private LocalDate date = LocalDate.now();
	   
	   @ManyToOne
		@JoinColumn(name = "idUser")
		private AppUser user;
	   
	   @ManyToOne
		//@JoinColumn(name = "idClient")
		private TypeReclamation typeReclamation;

	   
	   @ManyToOne
		//@JoinColumn(name = "idClient")
		private Client client;

	

	public Integer getIdReclamation() {
		return idReclamation;
	}

	public void setIdReclamation(Integer idReclamation) {
		this.idReclamation = idReclamation;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public TypeReclamation getTypeReclamation() {
		return typeReclamation;
	}

	public void setTypeReclamation(TypeReclamation typeReclamation) {
		this.typeReclamation = typeReclamation;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	

	public Réclamation(Integer idReclamation, String sujet, String description, LocalDate date, AppUser user,
			TypeReclamation typeReclamation, Client client) {
		super();
		this.idReclamation = idReclamation;
		this.sujet = sujet;
		this.description = description;
		this.date = date;
		//this.user = user;
		//this.typeReclamation = typeReclamation;
		//this.client = client;
	}

	public Réclamation() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	   
}