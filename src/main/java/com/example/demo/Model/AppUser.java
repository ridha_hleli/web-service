package com.example.demo.Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class AppUser implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUser;
	private String nom;
	private String avatar;
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private Role rolename;
	private String prenom;
	private String email;
	private String username;
	private String password;
	private String adresse;	
	private Integer deletedBy;
	private LocalDate  deletedAt;
	private Integer deleted;


	
	@JsonIgnore
    @OneToMany(mappedBy = "user" ,cascade = { CascadeType.MERGE })
	private List<Réclamation> reclamation;
	
	@JsonIgnore
    @OneToMany(mappedBy = "user" ,cascade = { CascadeType.MERGE })
	private List<Commande> commands;

	@JsonIgnore
    @OneToMany(mappedBy = "user" ,cascade = { CascadeType.MERGE })
	private List<Client> client;
	
	@JsonIgnore
    @OneToMany(mappedBy = "user" ,cascade = { CascadeType.MERGE })
	private List<Visite> visites;
	
	@JsonIgnore
    @OneToMany(mappedBy = "user" ,cascade = { CascadeType.MERGE })
	private List<Objectif> objectif;
	
	
	@ManyToOne(cascade = { CascadeType.MERGE })
//	@JoinColumn(name = "idCatalogue")
	private Catalogues catalogue;
	
	
	@OneToMany
	private List<Réglement> reg;
	
	public AppUser() {
		super();
	}

	public List<Visite> getVisites() {
		return visites;
	}

	public void setVisites(List<Visite> visites) {
		this.visites = visites;
	}


	public Catalogues getCatalogue() {
		return catalogue;
	}

	public void setCatalogue(Catalogues catalogue) {
		this.catalogue = catalogue;
	}

	public List<Réglement> getReg() {
		return reg;
	}

	public void setReg(List<Réglement> reg) {
		this.reg = reg;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Role getRolename() {
		return rolename;
	}

	public void setRolename(Role rolename) {
		this.rolename = rolename;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Integer getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	public LocalDate  getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(LocalDate  deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	public List<Réclamation> getReclamation() {
		return reclamation;
	}

	public void setReclamation(List<Réclamation> reclamation) {
		this.reclamation = reclamation;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public List<Commande> getCommands() {
		return commands;
	}

	public void setCommands(List<Commande> commands) {
		this.commands = commands;
	}

	public List<Client> getClient() {
		return client;
	}

	public void setClient(List<Client> client) {
		this.client = client;
	}

	public AppUser(Integer idUser, String nom, String avatar, Role rolename, String prenom, String email,
			String username, String password, String adresse, Integer deletedBy, LocalDate deletedAt, Integer deleted,
			List<Réclamation> reclamation, List<Visite> visites, Catalogues catalogue, List<Réglement> reg) {
		super();
		this.idUser = idUser;
		this.nom = nom;
		this.avatar = avatar;
		this.rolename = rolename;
		this.prenom = prenom;
		this.email = email;
		this.username = username;
		this.password = password;
		this.adresse = adresse;
		this.deletedBy = deletedBy;
		this.deletedAt = deletedAt;
		this.deleted = deleted;
	}

	public AppUser(String nom, String prenom, String userName, String encode, String email, Role role) {
		this.nom = nom;
		this.prenom = prenom;
		this.username = userName;
		this.password = encode;
		this.email = email;
		this.rolename = role;
	}


}