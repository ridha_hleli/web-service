package com.example.demo.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Value;

@Entity
public class ligne_commande implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long idligneCmd;
	  private int qte;
      private String nomProduit;
	  private String codeProduit;
	  private float montant;
      private LocalDateTime passedAt= LocalDateTime.now();
	  // private float prixTTC;
 	  // private int RemisePourcentage;
 	  // private float MontantRemise;
  
     
     
	@ManyToOne
	//@JoinColumn(name = "idCommande")
	Commande cmd;

	@ManyToOne
	//@JoinColumn(name = "idProduit")
	Produit prod;
      
       
       
	public Long getIdligneCmd() {
		return idligneCmd;
	}

	public void setIdligneCmd(Long idligneCmd) {
		this.idligneCmd = idligneCmd;
	}

	public Commande getCmd() {
		return cmd;
	}

	public void setCmd(Commande cmd) {
		this.cmd = cmd;
	}

	public Produit getProd() {
		return prod;
	}

	public void setProd(Produit prod) {
		this.prod = prod;
	}

	public ligne_commande() {
		super();
	}
	
	  public String getNomProduit() {
		return nomProduit;
	}

	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}

	public String getCodeProduit() {
		return codeProduit;
	}

	public void setCodeProduit(String codeProduit) {
		this.codeProduit = codeProduit;
	}

	public int getQte() { return qte; }
	  
	  public void setQte(int qte) { this.qte = qte; }

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	public LocalDateTime getPassedAt() {
		return passedAt;
	}

	public void setPassedAt(LocalDateTime passedAt) {
		this.passedAt = passedAt;
	}

	
}
