package com.example.demo.Model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
@Entity
public class Tarification {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTarif;
	private String nomTarif;
	private Date Date_Debut;
	private Date Date_fin;
	
	@OneToMany(fetch = FetchType.LAZY, 
		    cascade = CascadeType.ALL)
      @JoinColumn(name="idTarif",referencedColumnName = "idTarif")
      private List<Tarification_Prod_Client>TPC ;
      

  	@OneToMany(fetch = FetchType.LAZY, 
  		    cascade = CascadeType.ALL)
        @JoinColumn(name="idTarif",referencedColumnName = "idTarif")
        private List<Client> client;
  	
  	
	@OneToMany(fetch = FetchType.LAZY, 
  		    cascade = CascadeType.ALL)
        @JoinColumn(name="idTarif",referencedColumnName = "idTarif")
        private List<Produit> prod;
	
	
	public Long getIdTarif() {
		return idTarif;
	}
	public void setIdTarif(Long idTarif) {
		this.idTarif = idTarif;
	}
	public String getNomTarif() {
		return nomTarif;
	}
	public void setNomTarif(String nomTarif) {
		this.nomTarif = nomTarif;
	}
	public Date getDate_Debut() {
		return Date_Debut;
	}
	public void setDate_Debut(Date date_Debut) {
		Date_Debut = date_Debut;
	}
	public Date getDate_fin() {
		return Date_fin;
	}
	public void setDate_fin(Date date_fin) {
		Date_fin = date_fin;
	}
	public Tarification() {
		super();
	}
	
	
	
	
}