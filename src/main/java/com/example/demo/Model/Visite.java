package com.example.demo.Model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Visite implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Integer num;
	private String description;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;
	
	@JsonFormat(pattern="HH:mm")
    private LocalTime start;
    
	
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	private LocalDateTime passedAt = LocalDateTime.now();
    
    
    @ManyToOne
	//@JoinColumn(name = "idUser")
	private AppUser user;
    
    @ManyToOne
	//@JoinColumn(name = "idUser")
	private TypeVisite typeVisite;
   
   @ManyToOne
   //@JoinColumn(name = "idUser")
	private Client client;

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public Integer getNum() {
	return num;
}

public void setNum(Integer num) {
	this.num = num;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public LocalDate getDate() {
	return date;
}

public void setDate(LocalDate date) {
	this.date = date;
}

public LocalTime getStart() {
	return start;
}

public void setStart(LocalTime start) {
	this.start = start;
}


public LocalDateTime getPassedAt() {
	return passedAt;
}

public void setPassedAt(LocalDateTime passedAt) {
	this.passedAt = passedAt;
}

public AppUser getUser() {
	return user;
}

public void setUser(AppUser user) {
	this.user = user;
}

public TypeVisite getTypeVisite() {
	return typeVisite;
}

public void setTypeVisite(TypeVisite typeVisite) {
	this.typeVisite = typeVisite;
}

public Client getClient() {
	return client;
}

public void setClient(Client client) {
	this.client = client;
}

public Visite(Long id, Integer num, String description, LocalDate date, LocalTime start,
		LocalDateTime passedAt, AppUser user, TypeVisite typeVisite, Client client) {
	super();
	this.id = id;
	this.num = num;
	this.description = description;
	this.date = date;
	this.start = start;
	this.passedAt = passedAt;
	this.user = user;
	this.typeVisite = typeVisite;
	this.client = client;
}

public Visite() {
	super();
	// TODO Auto-generated constructor stub
}

   
   }   