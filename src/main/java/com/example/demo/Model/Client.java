package com.example.demo.Model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Client implements Serializable{
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idClient;
	private String nom;
	private String prenom;
	private String email;
	private String adresse;
	private String pattente;
	private String tel;
	private String fax;
	//private String nomCommerciale;
	private String avatar;
	
	
	@ManyToOne
	  @JoinColumn(name = "idUser")
	private AppUser user;
	
	@JsonIgnore
    @OneToMany(mappedBy = "client" ,cascade = { CascadeType.MERGE })
	private List<Visite> visites;
	
	@JsonIgnore
    @OneToMany(mappedBy = "client" ,cascade = { CascadeType.MERGE })
	private List<Commande> commands;
	
	
	
	
	public Integer getIdClient() {
		return idClient;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPattente() {
		return pattente;
	}

	public void setPattente(String pattente) {
		this.pattente = pattente;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
		
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public List<Visite> getVisites() {
		return visites;
	}

	public void setVisites(List<Visite> visites) {
		this.visites = visites;
	}

	public List<Commande> getCommands() {
		return commands;
	}

	public void setCommands(List<Commande> commands) {
		this.commands = commands;
	}

	public Client() {
		super();
	}


	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email
				+ ", adresse=" + adresse + ", pattente=" + pattente + ", tel=" + tel + ", fax=" + fax + "]";
	}

	public Client(Integer idClient, String nom, String prenom, String email, String adresse, String pattente,
			String tel, String fax, String nomCommerciale, String avatar, List<Visite> visites) {
		super();
		this.idClient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.adresse = adresse;
		this.pattente = pattente;
		this.tel = tel;
		this.fax = fax;
		this.avatar = avatar;
	}




}
