package com.example.demo.Model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity

public class Catalogues implements Serializable {
  @Id
  @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long idCatalogue;
    private String nomCatalogues;
    
	@JsonIgnore
    @OneToMany(mappedBy = "catalogue" ,cascade = { CascadeType.MERGE })
	private List<AppUser> user;

	
	@ManyToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE })
	@JoinTable(name = "LigneCatalogue", joinColumns = {
			@JoinColumn(name = "idCatalogue", referencedColumnName = "idCatalogue", nullable = false, updatable = true) }, inverseJoinColumns = {
					@JoinColumn(name = "idProduit", referencedColumnName = "idProduit", nullable = false, updatable = true) })
	private List<Produit> prod;	
	
	
	public List<AppUser> getUser() {
		return user;
	}
	public void setUser(List<AppUser> user) {
		this.user = user;
	}
	public Long getIdCatalogue() {
		return idCatalogue;
	}
	public void setIdCatalogue(Long idCatalogue) {
		this.idCatalogue = idCatalogue;
	}
	public String getNomCatalogues() {
		return nomCatalogues;
	}
	public void setNomCatalogues(String nomCatalogues) {
		this.nomCatalogues = nomCatalogues;
	}

	public Catalogues(String nomCatalogues) {
		super();
		this.nomCatalogues = nomCatalogues;
	}
	public Catalogues(String nomCatalogues, List<Produit> prods) {
		super();
		this.nomCatalogues = nomCatalogues;
	}
	
	public List<Produit> getProd() {
		return prod;
	}
	public void setProd(List<Produit> prod) {
		this.prod = prod;
	}
	public Catalogues() {
		super();
	}
    
}
