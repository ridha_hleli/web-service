package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Tarification_Prod_Client {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long idTarifProd;
		
		
		

		public Long getIdTarifProd() {
			return idTarifProd;
		}

		public void setIdTarifProd(Long idTarifProd) {
			this.idTarifProd = idTarifProd;
		}

		public Tarification_Prod_Client() {
			super();
		}
		
		
}
