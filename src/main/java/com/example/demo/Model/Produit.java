package com.example.demo.Model;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Produit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProduit;
	private String typeProduit;
	private String nomProduit;
	private float prix;
	private String designiation;
	private String codeProduit;
	private Integer poids;
	private String reference;
	private Date dateCreation;
	private Date dateExpiration;
	private String fileName;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime passedAt = LocalDateTime.now();
	//private float prixHT;
	//private float prixTVA;
	//private float prixTTC;
	
	
	@JsonIgnore
	@OneToMany(mappedBy = "prod", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ligne_commande> lc;
	
	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "idCategorie")
	private Categories categorie;
	
	
   @ManyToMany
	@JsonIgnore
	@JoinTable(name = "LigneCatalogue", joinColumns = {
			@JoinColumn(name = "idProduit", referencedColumnName = "idProduit", nullable = false, updatable = true) }, inverseJoinColumns = {
					@JoinColumn(name = "idCatalogue", referencedColumnName = "idCatalogue", nullable = false, updatable = true) })
	private List<Catalogues> catalogues;

	
   @ManyToMany
    @JsonIgnore
	@JoinTable(name = "LigneFacture", joinColumns = {
			@JoinColumn(name = "idProduit", referencedColumnName = "idProduit", nullable = false, updatable = true) }, inverseJoinColumns = {
					@JoinColumn(name = "idFacture", referencedColumnName = "idFacture", nullable = false, updatable = true) })
	private List<Facture> facts;

	
	public List<Facture> getFacts() {
		return facts;
	}

	public void setFacts(List<Facture> facts) {
		this.facts = facts;
	}
	
	public String getTypeProduit() {
		return typeProduit;
	}

	public List<Catalogues> getCatalogues() {
		return catalogues;
	}

	public void setCatalogues(List<Catalogues> catalogues) {
		this.catalogues = catalogues;
	}

	public Long getIdProduit() {
		return idProduit;
	}

	public void setIdProduit(Long idProduit) {
		this.idProduit = idProduit;
	}

	
	public Categories getCategorie() {
		return categorie;
	}

	public void setCategorie(Categories categorie) {
		this.categorie = categorie;
	}

	public void setTypeProduit(String typeProduit) {
		this.typeProduit = typeProduit;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public String getDesigniation() {
		return designiation;
	}

	public void setDesigniation(String designiation) {
		this.designiation = designiation;
	}

	public String getCodeProduit() {
		return codeProduit;
	}

	public void setCodeProduit(String codeProduit) {
		this.codeProduit = codeProduit;
	}

	public Integer getPoids() {
		return poids;
	}

	public void setPoids(Integer poids) {
		this.poids = poids;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getNomProduit() {
		return nomProduit;
	}

	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}

	public LocalDateTime getPassedAt() {
		return passedAt;
	}

	public void setPassedAt(LocalDateTime passedAt) {
		this.passedAt = passedAt;
	}


	public Produit(String typeProduit, float prix, String designiation,
			String codeProduit, Integer poids, String reference, Date dateCreation, Date dateExpiration,
			 String nomProduit, String fileName, LocalDateTime passedAt) {
		super();
		this.idProduit = idProduit;
		this.typeProduit = typeProduit;
		this.prix = prix;
		this.designiation = designiation;
		this.codeProduit = codeProduit;
		this.poids = poids;
		this.reference = reference;
		this.dateCreation = dateCreation;
		this.dateExpiration = dateExpiration;
		this.nomProduit = nomProduit;
		this.fileName = fileName;
		this.passedAt = passedAt;

	}


	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<ligne_commande> getLc() {
		return lc;
	}

	public void setLc(List<ligne_commande> lc) {
		this.lc = lc;
	}

	@Override
	public String toString() {
		return "Produit [idProduit=" + idProduit + ", typeProduit=" + typeProduit + ", prix=" + prix + ", designiation="
				+ designiation + ", codeProduit=" + codeProduit + ", poids=" + poids + ", reference=" + reference
				+ ", dateCreation=" + dateCreation + ", dateExpiration=" + dateExpiration + ", nomProduit=" + nomProduit
				+ "]";
	}

}
