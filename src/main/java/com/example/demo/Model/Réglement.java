package com.example.demo.Model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Réglement implements Serializable {
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer idReg;
   private float montant;
   private Date dateReg;
   
   
   @ManyToMany
   @JsonIgnore
	 @JoinTable(name = "ReglementFacture",
   joinColumns = {
           @JoinColumn(name = "idReg", referencedColumnName = "idReg",
                   nullable = false, updatable = true)},
   inverseJoinColumns = {
           @JoinColumn(name = "idFacture", referencedColumnName = "idFacture",
                   nullable = false, updatable = true)})
	private List<Facture> facts;
   
   
   
   
      public List<Facture> getFacts() {
	    return facts; }
      
      public void setFacts(List<Facture> facts) {
	     this.facts = facts;}
      
      public Integer getIdReg() {
	      return idReg;}
      
      public void setIdReg(Integer idReg) {
	    this.idReg = idReg;}

      public float getMontant() {
        	return montant;}

      public void setMontant(float montant) {
	     this.montant = montant;}
      
      public Date getDateReg() {
	     return dateReg;}
      
    public void setDateReg(Date dateReg) {
	   this.dateReg = dateReg;}
    
     
	public Réglement(float montant, Date dateReg, List<Facture> facts) {
		super();
		this.montant = montant;
		this.dateReg = dateReg;
		this.facts = facts;
	}

	public Réglement() {
		super();
	}
       
       
   
     
   
}
