package com.example.demo.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
@Entity
public class TypeReglement {
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTypeReg;
	private String TypeName;
	
	@OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	@JoinColumn(name="idTypeReg",referencedColumnName = "idTypeReg")
	private List<Réglement> reg;

	public Integer getIdTypeReg() {
		return idTypeReg;
	}

	public void setIdTypeReg(Integer idTypeReg) {
		this.idTypeReg = idTypeReg;
	}

	public String getTypeName() {
		return TypeName;
	}

	public void setTypeName(String typeName) {
		TypeName = typeName;
	}

	public List<Réglement> getReg() {
		return reg;
	}

	public void setReg(List<Réglement> reg) {
		this.reg = reg;
	}

	public TypeReglement(String typeName, List<Réglement> reg) {
		super();
		TypeName = typeName;
		this.reg = reg;
	}

	public TypeReglement() {
		super();
	}
	
	
}
