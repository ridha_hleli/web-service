package com.example.demo.Model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Objectif {
	
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Integer idObjetif;
	   private float objectif;
	   @JsonFormat(pattern="yyyy_MM_dd")
	   private LocalDate dateDebut = LocalDate.now();
	   @JsonFormat(pattern="yyyy_MM_dd")
	   private LocalDate dateFin = LocalDate.now();
	   
	   
	   @ManyToOne
		//@JoinColumn(name = "idCommande")
		AppUser user;
	   
	   
	public Integer getIdObjetif() {
		return idObjetif;
	}
	
	public void setIdObjetif(Integer idObjetif) {
		this.idObjetif = idObjetif;
	}
	
	public float getObjectif() {
		return objectif;
	}
	
	public void setObjectif(float objectif) {
		this.objectif = objectif;
	}
	
	public LocalDate getDateDebut() {
		return dateDebut;
	}
	
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}
	
	public LocalDate getDateFin() {
		return dateFin;
	}
	
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}
	
	
	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public Objectif(Integer idObjetif, float objectif, LocalDate dateDebut, LocalDate dateFin) {
		super();
		this.idObjetif = idObjetif;
		this.objectif = objectif;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	
	public Objectif() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	   
	   
	   
	   
	   

}
