package com.example.demo.Model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class TypeVisite implements Serializable{
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTypeVisite;
	private String TypeName;
	
	
	@JsonIgnore
    @OneToMany(mappedBy = "typeVisite" ,cascade = { CascadeType.MERGE })
	private List<Visite> visites;
	
	
	public Integer getIdTypeVisite() {
		return idTypeVisite;
	}
	
	public void setIdTypeVisite(Integer idTypeVisite) {
		this.idTypeVisite = idTypeVisite;
	}
	
	public String getTypeName() {
		return TypeName;
	}
	
	public void setTypeName(String typeName) {
		TypeName = typeName;
	}

	public TypeVisite(Integer idTypeVisite, String typeName) {
		super();
		this.idTypeVisite = idTypeVisite;
		TypeName = typeName;
	}

	public TypeVisite() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
}
