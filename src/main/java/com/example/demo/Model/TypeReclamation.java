package com.example.demo.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
@Entity
public class TypeReclamation {
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTypeRec;
	private String TypeName;
	
	
	public Integer getIdTypeRec() {
		return idTypeRec;
	}
	public void setIdTypeRec(Integer idTypeRec) {
		this.idTypeRec = idTypeRec;
	}
	public String getTypeName() {
		return TypeName;
	}
	public void setTypeName(String typeName) {
		TypeName = typeName;
	}
	
	
	public TypeReclamation(Integer idTypeRec, String typeName) {
		super();
		this.idTypeRec = idTypeRec;
		TypeName = typeName;
	}
	
	public TypeReclamation() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	
	
}
