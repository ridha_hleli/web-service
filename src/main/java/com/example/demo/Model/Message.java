package com.example.demo.Model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
public class Message {
	
	@ Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Integer idMesage;
	   private String contenu;
	   private String fileName;
	   @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	   @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	   private LocalDateTime passedAt = LocalDateTime.now();
	   
	   @ManyToOne
		//@JoinColumn(name = "idRecepteur")
		private AppUser user1;
	   
	   @ManyToOne
		//@JoinColumn(name = "idEmetteur")
		private AppUser user2;

	public Integer getIdMesage() {
		return idMesage;
	}

	public void setIdMesage(Integer idMesage) {
		this.idMesage = idMesage;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public LocalDateTime getPassedAt() {
		return passedAt;
	}

	public void setPassedAt(LocalDateTime passedAt) {
		this.passedAt = passedAt;
	}

	public AppUser getUser1() {
		return user1;
	}

	public void setUser1(AppUser user1) {
		this.user1 = user1;
	}

	public AppUser getUser2() {
		return user2;
	}

	public void setUser2(AppUser user2) {
		this.user2 = user2;
	}

	public Message(Integer idMesage, String contenu, String fileName, LocalDateTime passedAt, AppUser user1,
			AppUser user2) {
		super();
		this.idMesage = idMesage;
		this.contenu = contenu;
		this.fileName = fileName;
		this.passedAt = passedAt;
		this.user1 = user1;
		this.user2 = user2;
	}

	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}
	   
	   

}
