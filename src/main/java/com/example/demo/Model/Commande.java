package com.example.demo.Model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Commande implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Long idCommande;
	   private Long numCmd;
	   private String etatCmd;
	   private float montant_total;
	   // private float prixHT;
	   //private float prixTVA;
	   //private float prixTTC;
	   @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	   @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	   private LocalDateTime passedAt = LocalDateTime.now();
	   @JsonFormat(pattern="yyyy_MM_dd")
	   @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	   private LocalDate date = LocalDate.now();
	
	 
	  
	   @ManyToOne
		//@JoinColumn(name = "idCommande")
		AppUser user;
	   
	   @ManyToOne
		//@JoinColumn(name = "idCommande")
		Client client;
	   
	   @OneToMany (mappedBy = "cmd", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	   @JsonIgnore
		private List<ligne_commande> lc;
		
		
	
	public Long getIdCommande() {
		return idCommande;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public void setIdCommande(Long idCommande) {
		this.idCommande = idCommande;
	}
	public Long getNumCmd() {
		return numCmd;
	}
	public void setNumCmd(Long numCmd) {
		this.numCmd = numCmd;
	}
	public String getEtatCmd() {
		return etatCmd;
	}
	public void setEtatCmd(String etatCmd) {
		this.etatCmd = etatCmd;
	}
	public LocalDateTime getPassedAt() {
		return passedAt;
	}
	public void setPassedAt(LocalDateTime passedAt) {
		this.passedAt = passedAt;
	}
	
	public Float getMontant_total() {
		return montant_total;
	}
	public void setMontant_total(Float montant_total) {
		this.montant_total = montant_total;
	}
	  public LocalDate getDate() {
			return date;
		}
		public void setDate(LocalDate date) {
			this.date = date;
		}
	public List<ligne_commande> getLc() {
		return lc;
	}
	public void setLc(List<ligne_commande> lc) {
		this.lc = lc;
	}
	
		public void setMontant_total(float montant_total) {
			this.montant_total = montant_total;
		}
		public Commande(Long idCommande, Long numCmd, String etatCmd, float montant_total, LocalDateTime passedAt,
				LocalDate date, AppUser user, Client client, List<ligne_commande> lc) {
			super();
			this.idCommande = idCommande;
			this.numCmd = numCmd;
			this.etatCmd = etatCmd;
			this.montant_total = montant_total;
			this.passedAt = passedAt;
			this.date = date;
			this.user = user;
			this.client = client;
			this.lc = lc;
		}
		public Commande() {
			super();
			// TODO Auto-generated constructor stub
		}
	
	
}
