package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Réclamation;
import com.example.demo.Model.Tache;
import com.example.demo.Repository.reclamationRepository;
import com.example.demo.Repository.tacheRepository;

@Service
@Transactional
public class ReclamationService {

	private final reclamationRepository repo;
	private final tacheRepository repoTache;

	@Autowired
	public ReclamationService(reclamationRepository repo, tacheRepository repoTache) {
		super();
		this.repo = repo;
		this.repoTache = repoTache;
	}
	
	 public Réclamation addReclamtion(Réclamation rec) {
		 List<Tache> taches = this.repoTache.findByClientAndDate(rec.getClient(), rec.getDate());
			taches.stream().forEach(tache -> {
				tache.setEtat(true);
				this.repoTache.save(tache);
			});
         return repo.save(rec);
   }

     public List<Réclamation> findAllRéclamation() {
       return repo.findAll();
   }

   public Réclamation updateRéclamation(Réclamation rec) {
      return repo.save(rec);
    }


   public Réclamation findRéclamationByIdRéclamation(Integer idRec) {
    	return repo.findById(idRec)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Réclamation by id " + idRec + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteReclamation(Integer idRec){
       repo.deleteById(idRec);
      }
}