package com.example.demo.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.Model.AppUser;
import com.example.demo.Model.Catalogues;
import com.example.demo.Model.Client;
import com.example.demo.Model.Commande;
import com.example.demo.Repository.appUserRepository;
import com.example.demo.Repository.cataloguesRepository;
import com.example.demo.dto.ClientStatInfo;
import com.example.demo.dto.UserStatInfo;


@Service
@Transactional
public class UserService {
	private final appUserRepository repo;
	
    @Autowired
	public UserService(appUserRepository repo ) {
		super();
		this.repo = repo;
	}
    public AppUser addUser(AppUser user) {
              return repo.save(user);
    }
    
    public List<UserStatInfo> stat() {
		 
		 List<UserStatInfo> userStatInfos = new ArrayList<>();
		 List<AppUser> users = this.repo.findAll();
		 users.stream().forEach(user -> 
		 userStatInfos.add(new UserStatInfo(user.getNom(), calculateMontantTotalGlobale(user.getCommands()))));
        return userStatInfos;
  }

    private float calculateMontantTotalGlobale(List<Commande> commands) {	 
   	 // boucle for commands 
         float result = commands.stream()
   	     .map(command -> command.getMontant_total()).reduce(0.f , (a, b) -> a + b);
         
		    return result;
	}

    public List<AppUser> findAllAppUsers() {
        return repo.findAll();
    }
	
    public AppUser updateAppUser(AppUser user) {
        return repo.save(user);
    }
    
    public AppUser findUserByIdUser(Integer idUser) {
    	return repo.findById(idUser)
                .orElseThrow(() -> new com.example.demo.Exception.UserNotFoundException("User by id " + idUser + " was not found"));
    }
    
    public AppUser findUserByUserName(String username) {
    	return repo.findByUsername(username)
    			.orElseThrow(() -> new com.example.demo.Exception.UserNotFoundException("User by username " + username + " was not found"));
    }
               


    public Long count() {

        return repo.count();
    }

    public void deleteUser(Integer idUser){
        repo.deleteUserByIdUser(idUser);
    }
    
    public AppUser getCat(Integer idUser) {
		return repo.getcatalogbyuser(idUser);
	}
	

}
