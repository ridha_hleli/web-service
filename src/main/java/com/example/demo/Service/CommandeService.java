package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Client;
import com.example.demo.Model.Commande;
import com.example.demo.Model.Tache;
import com.example.demo.Repository.clientRepository;
import com.example.demo.Repository.commandeRepository;
import com.example.demo.Repository.tacheRepository;

@Service
@Transactional
public class CommandeService {

	private final commandeRepository repo;
	private final tacheRepository repoTache;

	@Autowired
	public CommandeService(commandeRepository repo, tacheRepository repoTache) {
		super();
		this.repo = repo;
		this.repoTache = repoTache;
	}

	public Commande addCommande(Commande cmd) {
		List<Tache> taches = this.repoTache.findByClientAndDate(cmd.getClient(), cmd.getDate());
		taches.stream().forEach(tache -> {
			tache.setEtat(true);
			this.repoTache.save(tache);
		});

		return repo.save(cmd);
	}

	public List<Commande> findAllCommandes() {
		return repo.findAll();
	}

	public Commande updateCommande(Commande Cmd) {
		return repo.save(Cmd);
	}

	public Commande findCommandeByIdCommande(Long idCommande) {
		return repo.findCommandeByIdCommande(idCommande) .orElseThrow(() -> new com.example.demo.Exception.CommandeNotFoundException(
						"Commande by id " + idCommande + " was not found"));
	}

	public Long count() {

		return repo.count();
	}

	public void deleteCommande(Long idCommande) {
		Commande commandeToBeDeleted = this.findCommandeByIdCommande(idCommande);
		List<Tache> taches = this.repoTache.findByClientAndDate(commandeToBeDeleted.getClient(),
				commandeToBeDeleted.getDate());
		taches.stream().forEach(tache -> {
			tache.setEtat(false);
			this.repoTache.save(tache);
		});
		repo.deleteCommandeByIdCommande(idCommande);
	}
}
