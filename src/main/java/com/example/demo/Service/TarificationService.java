package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Tarification;
import com.example.demo.Repository.tarificationRepository;

@Service
@Transactional
public class TarificationService {
	private final tarificationRepository repo;

    @Autowired
	public TarificationService(tarificationRepository repo) {
		super();
		this.repo = repo;
	}
    public Tarification addtarif(Tarification tarif) {
              return repo.save(tarif);
    }

    public List<Tarification> findAllTarifications() {
        return repo.findAll();
    }
	
    public Tarification updateTarification(Tarification tarif) {
        return repo.save(tarif);
    }
    public Tarification findtarifByIdtarif(Long idtarif) {
    	return repo.findById(idtarif)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("tarif by id " + idtarif + " was not found"));
    }

    public Long count() {

        return repo.count();
    }

    public void deletetarif(Long idtarif){
        repo.deleteById(idtarif);
    }

}
