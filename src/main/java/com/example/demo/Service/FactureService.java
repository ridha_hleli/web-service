package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Facture;
import com.example.demo.Repository.factureRepository;

@Service
@Transactional
public class FactureService {
	private final factureRepository repo;

	@Autowired
	public FactureService(factureRepository repo) {
		super();
		this.repo = repo;
	}
	
	 public Facture addFacture(Facture facture) {
         return repo.save(facture);}

     public List<Facture> findAllFactures() {
       return repo.findAll(); }

   public Facture updateFacture(Facture facture) {
      return repo.save(facture);  }


   public Facture findFactureByIdfacture(Integer idFacture) {
    	return repo.findById(idFacture)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Facture by id " + idFacture + " was not found")); }


    public Long count() {

     return repo.count();  }

    public void deleteFacture(Integer idFacture){
       repo.deleteById(idFacture);
      }
}
