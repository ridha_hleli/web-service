package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Réglement;
import com.example.demo.Repository.reglementRepository;
import com.example.demo.Repository.visiteRepository;

@Service
@Transactional
public class ReglementService {

	private final reglementRepository repo;

	@Autowired
	public ReglementService(reglementRepository repo) {
		super();
		this.repo = repo;
	}
	 public Réglement addReglement(Réglement reg) {
         return repo.save(reg);
   }

     public List<Réglement> findAllReglement() {
       return repo.findAll();
   }

   public Réglement updateReglement(Réglement reg) {
      return repo.save(reg);
    }


   public Réglement findByIdReglement(Integer idReg) {
    	return repo.findById(idReg)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Reglement by id " + idReg + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteReglement(Integer idReg){
       repo.deleteById(idReg);
      }
}