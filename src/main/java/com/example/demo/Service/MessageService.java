package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Message;
import com.example.demo.Repository.messageRepository;

@Service
@Transactional
public class MessageService {
	
	private final messageRepository repo;

	
	@Autowired
	public MessageService(messageRepository repo) {
		super();
		this.repo = repo;
	}
	
	
	 public Message addMessage(Message mesg) {
         return repo.save(mesg);
     }


	public List<Message> findAllMessage() {
       return repo.findAll();
    }

   public Message updateMessage(Message mesg) {
      return repo.save(mesg);
    }


   public Message findMessageByIdMessage(Integer idMesg) {
    	return repo.findById(idMesg)
        .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Message by id " + idMesg + " was not found"));
    }


    public Long count() {
      return repo.count();
     }

    public void deleteMessage(Integer idMesg){
       repo.deleteById(idMesg);
      }
}

