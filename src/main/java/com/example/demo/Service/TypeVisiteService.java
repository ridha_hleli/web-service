package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.TypeVisite;
import com.example.demo.Repository.typeVisiteRepository;

@Service
@Transactional
public class TypeVisiteService {

	private final typeVisiteRepository repo;

	@Autowired
	public TypeVisiteService(typeVisiteRepository repo) {
		super();
		this.repo = repo;
	}
	 public TypeVisite addTypeVisite(TypeVisite typeV) {
         return repo.save(typeV);
   }

     public List<TypeVisite> findAllTypeVisite() {
       return repo.findAll();
   }

   public TypeVisite updateTypeVisite(TypeVisite typeV) {
      return repo.save(typeV);
    }


   public TypeVisite findTypeVisiteById(Integer idTypeV) {
    	return repo.findById(idTypeV)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("TypeVisite by id " + idTypeV + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteTypeVisite(Integer idTypeV){
       repo.deleteById(idTypeV);
      }
}