package com.example.demo.Service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Client;
import com.example.demo.Model.Commande;
import com.example.demo.Repository.clientRepository;
import com.example.demo.dto.ClientStatInfo;
import com.sun.el.stream.Optional;

@Service
@Transactional
public class ClientService {
	private final clientRepository repo;

	@Autowired
	public ClientService(clientRepository repo) {
		super();
		this.repo = repo;
	}
	
	 public Client addClient(Client client) {
         return repo.save(client);
   }
	 
		
	 public List<ClientStatInfo> stat() {
		 
		 List<ClientStatInfo> clientStatInfos = new ArrayList<>();
		 List<Client> clients = this.repo.findAll();
		 clients.stream().forEach(client -> 
		 clientStatInfos.add(new ClientStatInfo(client.getNom(), calculateMontantTotalGlobale(client.getCommands())))); 
         return clientStatInfos;
   }

     private float calculateMontantTotalGlobale(List<Commande> commands) {	 
    	 // boucle for commands 
          float result = commands.stream()
    	     .map(command -> command.getMontant_total()).reduce(0.f , (a, b) -> a + b);
          
		    return result;
	}

	public List<Client> findAllClients() {
       return repo.findAll();
   }

   public Client updateClient(Client client) {
      return repo.save(client);
    }


   public Client findClientByIdClient(Integer idClient) {
    	return repo.findClientByIdClient(idClient)
                .orElseThrow(() -> new com.example.demo.Exception.UserNotFoundException("Client by id " + idClient + " was not found"));
    }


    public Long count() {

     return repo.count();
     }
    
    public void deleteClient(Integer idClient){
       repo.deleteById(idClient);
      }
}
