package com.example.demo.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Exception.ObjectNotFound;
import com.example.demo.Model.AppUser;
import com.example.demo.Model.Catalogues;
import com.example.demo.Model.Produit;
import com.example.demo.Repository.appUserRepository;
import com.example.demo.Repository.cataloguesRepository;
import com.example.demo.Repository.prodRepository;

@Service
@Transactional
public class CatalogueService {
	@Autowired
	 cataloguesRepository repo;
	@Autowired
	 prodRepository repp;

	@Autowired
	appUserRepository repou;
	/*
	public Catalogues affecterCataloguesToUser(Long idCatalogue) {
		//Boolean cat =repp.existsById(idCatalogue);
		 Catalogues catalogue = repo.findByIdCatalogue(idCatalogue);
		 List<AppUser> user =repou.findAll();
	/*	if(cat == false) {
			
		return repo.save(catalogue);}
		else {//
			catalogue.setUser(user);
System.out.println(user);
			return repo.save(catalogue);
		}
	*/
	//ajouter produit to catalogue
	public Catalogues ajouterProduitToCatalogues(Catalogues catalogue,Long idCatalogue) {
			Catalogues catalog = repo.findByIdCatalogue(idCatalogue);

			if (catalog == null) {				
			return	repo.save(catalogue);
			} else {
				//exists prod or no
				catalog.setIdCatalogue(catalog.getIdCatalogue());
				System.out.println(catalog.getIdCatalogue());
	
				List<Produit> prod = repp.findAll();
				catalog.setProd(prod);
			return catalog;

			}}
	
/*
	public Catalogues ajouterCatalogues(Catalogues catalogue, Integer idUser	/*, Long prod_id_produit//) {
		Catalogues catalog = repo.findCatalogueByIdUser(idUser);
		List<AppUser> userr= repou.findUserByIdUser(idUser);
		//if(userr==null) {System.out.println("user not found");}
		if (catalog == null) {		
		catalogue.setUser(userr);	
		return	repo.save(catalogue);
		} else {
			//exists prod or no
			catalog.setIdCatalogue(catalog.getIdCatalogue());
			System.out.println(catalog.getIdCatalogue());
			
			List<AppUser> user= repou.findUserByIdUser(idUser);
			catalog.setUser(user);
			
			List<Produit> prod = repp.findAll();
			catalog.setProd(prod);
			
			
		return catalog;

		}
	}*/

	public Catalogues addCatalogues(Catalogues catalogue) {
		return repo.save(catalogue);
	}

	public List<Catalogues> findAllCatalogues() {
		return repo.findAll();
	}

	public Catalogues updateCatalogue(Catalogues catalogue) {
		return repo.save(catalogue);
	}

	public Catalogues findCatalogueByIdCatalogue(Long idcatalogue) {
		return repo.findById(idcatalogue).orElseThrow(() -> new com.example.demo.Exception.CatalogueNotFoundException(
				"Catalogue by id " + idcatalogue + " was not found"));
	}

	public Long count() {

		return repo.count();
	}

	public void deleteCatalogue(Long idCatalogue) {
		repo.deleteById(idCatalogue);
	}

	public Catalogues findCatalogueByIdUser(Integer user_id_user) {
		return repo.findCatalogueByIdUser(user_id_user);
	}
	
	
}
