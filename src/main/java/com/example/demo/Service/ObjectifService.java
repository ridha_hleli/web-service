package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Objectif;
import com.example.demo.Repository.objectifRepository;

@Service
@Transactional
public class ObjectifService {
	
private final objectifRepository repo;

	
	@Autowired
	public ObjectifService(objectifRepository repo) {
		super();
		this.repo = repo;
	}
	
	
	 public Objectif addObjectif(Objectif obj) {
         return repo.save(obj);
     }


	public List<Objectif> findAllObjectif() {
       return repo.findAll();
    }

   public Objectif updateObjectif(Objectif obj) {
      return repo.save(obj);
    }
   
   public Objectif findObjectifByIdObjectif(Integer idObj) {
   	return repo.findById(idObj)
       .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Objectif by id " + idObj + " was not found"));
   }


}
