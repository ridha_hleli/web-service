package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Catalogues;
import com.example.demo.Model.TypeReglement;
import com.example.demo.Repository.typeRegRepository;
import com.example.demo.Repository.visiteRepository;

@Service
@Transactional
public class TypeReglementService {

	private final typeRegRepository repo;

	@Autowired
	public TypeReglementService(typeRegRepository repo) {
		super();
		this.repo = repo;
	}
	 public TypeReglement addTypeReg(TypeReglement typeReg) {
         return repo.save(typeReg);
   }

     public List<TypeReglement> findAllTypeReg() {
       return repo.findAll();
   }

   public TypeReglement updateTypeReg(TypeReglement typeReg) {
      return repo.save(typeReg);
    }


   public TypeReglement findTypeRegByIdTypeReg(Integer idTypeReg) {
    	return repo.findById(idTypeReg)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Type Reglement by id " + idTypeReg + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteTypeReg(Integer idTypeReg){
       repo.deleteById(idTypeReg);
      }
}