package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.TypeReclamation;
import com.example.demo.Repository.typeRecRepo;

@Service
@Transactional
public class TypeReclamationService {

	private final typeRecRepo repo;

	@Autowired
	public TypeReclamationService(typeRecRepo repo) {
		super();
		this.repo = repo;
	}
	 public TypeReclamation addTypeRec(TypeReclamation rec) {
         return repo.save(rec);
   }

     public List<TypeReclamation> findAllTypeReclamation() {
       return repo.findAll();
   }

   public TypeReclamation updateTypeReclamation(TypeReclamation rec) {
      return repo.save(rec);
    }


   public TypeReclamation findTypeReclamationById(Integer idTypeRec) {
    	return repo.findById(idTypeRec)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("TypeReclamation by id " + idTypeRec + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteTypeRec(Integer idTypeRec){
       repo.deleteById(idTypeRec);
      }
}