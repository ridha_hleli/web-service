package com.example.demo.Service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.Model.Produit;
import com.example.demo.Repository.prodRepository;


@Service
@Transactional
public class produitService {
	private final prodRepository repo;

	@Autowired
	public produitService(prodRepository repo) {
		super();
		this.repo = repo;
	}
	
	 public Produit addProduit(Produit prod) {
         return repo.save(prod);
   }

     public List<Produit> findAllProduits() {
       return repo.findAll();
   }

   public Produit updateProduit(Produit prod) {
      return repo.save(prod);
    }
    public Produit findProduitByIdProduit(Long idProduit) {
	   return repo.findById(idProduit)
           .orElseThrow(() -> new com.example.demo.Exception.ProduitNotFoundException("Produit by id " + idProduit + " was not found"));
       }

    public Long count() {

     return repo.count();
     }

    public void deleteProduit(Long idProduit){
       repo.deleteById(idProduit);
      }
    
    public Produit storeFile(MultipartFile file,String typeProduit, float prix, String designiation, String codeProduit, Integer poids,
			String reference, Date dateCreation, Date dateExpiration, String nomProduit,LocalDateTime passedAt) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		Produit produit = new Produit(typeProduit, prix, designiation, codeProduit, poids, reference, dateCreation, dateExpiration, nomProduit, fileName, passedAt);
          return repo.save(produit);
        }
    
    public Produit getFile(Long idProduit) {
        return repo.findById(idProduit)          
        		.orElseThrow(() -> new com.example.demo.Exception.ProduitNotFoundException("Produit by id " + idProduit + " was not found"));

    }

	

}
