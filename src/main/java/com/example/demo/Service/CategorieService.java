package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.Model.Categories;
import com.example.demo.Repository.categoriesRepository;

@Service
@Transactional
public class CategorieService {

	
		private final categoriesRepository repo;

		@Autowired
		public CategorieService(categoriesRepository repo) {
			super();
			this.repo = repo;
		}
		
		 public Categories addCategorie(Categories categorie) {
	         return repo.save(categorie);
	   }

	     public List<Categories> findAllCategories() {
	       return repo.findAll();
	   }

		   public Categories updateCategorie(Categories categorie) {
		      return repo.save(categorie);
		    }
		 
		 /*  public ResponseEntity<Categories> modifierCategorie(@PathVariable("id") Long idCategorie,@RequestBody Categories categorie) {
			   Optional<Categories> cat =repo.findById(idCategorie);
			  
			   if(cat.isPresent()) {
				  System.out.println(cat.toString());
			  Categories _cat=cat.get();
			   _cat.setNomCategorie(categorie.getNomCategorie());
			   _cat.setDescription(categorie.getDescription());
			
				return new ResponseEntity<>(repo.save(_cat), HttpStatus.OK);
				}
			  else{
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			  }
			  }*/

	   public Categories findCategorieByIdCategorie(Long idCategorie) {
	    	return repo.findCategoriesByIdCategorie(idCategorie)
	                .orElseThrow(() -> new com.example.demo.Exception.CategorieNotFoundException("Categorie by id " + idCategorie + " was not found"));
	    }


	    public Long count() {

	     return repo.count();
	     }

	    public void deleteCategorie(Long idCategorie){
	       repo.deleteCategoriesByIdCategorie(idCategorie);
	      }
}
	
	
	
	
	