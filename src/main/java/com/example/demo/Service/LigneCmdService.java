package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.ligne_commande;
import com.example.demo.Repository.LigneCmdRepo;
import com.example.demo.dto.UpdateLigneCommandeByQteRequest;

@Service
@Transactional
public class LigneCmdService {
	private final LigneCmdRepo repo;

	@Autowired
	public LigneCmdService(LigneCmdRepo repo) {
		super();
		this.repo = repo;
	}
	
	 public ligne_commande addligne_commande(ligne_commande lc) {
         return repo.save(lc);
         }

     public List<ligne_commande> findAllligne_commandes() {
       return repo.findAll(); 
       }

   public ligne_commande updateligne_commande(ligne_commande lc) {
      return repo.save(lc);  
      }

   public ligne_commande findligne_commandeByIdligneCmd(Long idligneCmd) {
    	return repo.findById(idligneCmd)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("ligne_commande by id " + idligneCmd + " was not found")); }

   
   public ligne_commande updateLigneCommandeQte(UpdateLigneCommandeByQteRequest updateLigneCommandeByQteRequest) {
		ligne_commande ligneCommande = repo.findById(updateLigneCommandeByQteRequest.getIdligneCmd())
				.orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound(
						"ligne_commande by id " + updateLigneCommandeByQteRequest.getIdligneCmd() + " was not found"));
		
		ligneCommande.setQte(updateLigneCommandeByQteRequest.getQte());
		ligneCommande.setMontant(updateLigneCommandeByQteRequest.getMontant());
		return repo.save(ligneCommande);

	}

    public Long count() {

     return repo.count();  }

    public void deleteligne_commande(Long idligneCmd){
       repo.deleteById(idligneCmd);
      }


}
