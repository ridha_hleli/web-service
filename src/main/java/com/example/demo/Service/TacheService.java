package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Tache;
import com.example.demo.Repository.tacheRepository;

@Service
@Transactional
public class TacheService {
	private final tacheRepository repo;

	@Autowired
	public TacheService(tacheRepository repo) {
		super();
		this.repo = repo;
	}
	 public Tache addTache(Tache tache) {
         return repo.save(tache);
   }

     public List<Tache> findAllTaches() {
       return repo.findAll();
   }

   public Tache updateTache(Tache tache) {
      return repo.save(tache);
    }


   public Tache findTacheById(Long idTache) {
    	return repo.findById(idTache)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Tache by id " + idTache + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteTache(Long idTache){
       repo.deleteById(idTache);
      }
}


	
