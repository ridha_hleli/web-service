package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Tache;
import com.example.demo.Model.Visite;
import com.example.demo.Repository.tacheRepository;
import com.example.demo.Repository.visiteRepository;
@Service
@Transactional
public class VisiteService {
	private final visiteRepository repo;
	private final tacheRepository repoTache;

	@Autowired
	public VisiteService(visiteRepository repo, tacheRepository repoTache) {
		super();
		this.repo = repo;
		this.repoTache = repoTache;
	}
	 public Visite addVisite(Visite visite) {
		   List<Tache> taches = this.repoTache.findByClientAndDate(visite.getClient(), visite.getDate());
			taches.stream().forEach(tache -> {
				tache.setEtat(true);
				this.repoTache.save(tache);
			});
         return repo.save(visite);
   }

     public List<Visite> findAllVisites() {
       return repo.findAll();
   }

   public Visite updateVisite(Visite visite) {
      return repo.save(visite);
    }


   public Visite findVisiteById(Long idVisite) {
    	return repo.findById(idVisite)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Visite by id " + idVisite + " was not found"));
    }


    public Long count() {

     return repo.count();
     }

    public void deleteVisite(Long idVisite){
       repo.deleteById(idVisite);
      }
}


	
