package com.example.demo.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Tarification_Prod_Client;
import com.example.demo.Repository.tarification_Prod_Client;

@Service
@Transactional
public class Tarif_Prod_ClientService {
	private final tarification_Prod_Client repo;

    @Autowired
	public Tarif_Prod_ClientService(tarification_Prod_Client repo) {
		super();
		this.repo = repo;
	}
    public Tarification_Prod_Client addTarifProdClient(Tarification_Prod_Client tpc) {
              return repo.save(tpc);
    }

    public List<Tarification_Prod_Client> findAllTarifProdClient() {
        return repo.findAll();
    }
	
    public Tarification_Prod_Client updateTarifProdClient(Tarification_Prod_Client tpc) {
        return repo.save(tpc);
    }
    public Tarification_Prod_Client findUserByIdUser(Long idTarifProd) {
    	return repo.findById(idTarifProd)
                .orElseThrow(() -> new com.example.demo.Exception.ObjectNotFound("Tarif Produit Client by id " + idTarifProd + " was not found"));
    }

    public Long count() {

        return repo.count();
    }

    public void deleteTarifProdClient(Long idTarifProd){
        repo.deleteById(idTarifProd);
    }

}
