package com.example.demo.dto;

public class UpdateLigneCommandeByQteRequest {
	
	private int qte;
	private long idligneCmd;
	private float montant;
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	public long getIdligneCmd() {
		return idligneCmd;
	}
	public void setIdligneCmd(long idligneCmd) {
		this.idligneCmd = idligneCmd;
	}
	public float getMontant() {
		return montant;
	}
	public void setMontant(float montant) {
		this.montant = montant;
	}
	
	

	
	

}
