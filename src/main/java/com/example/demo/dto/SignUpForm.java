package com.example.demo.dto;

import com.example.demo.Model.Role;

public class SignUpForm {
	

	private String nom;
	private String prenom;
	private String userName;
	private String password;
	private String email;
	private Role role;
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public SignUpForm(String nom, String prenom, String userName, String password, String email, Role role) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.role = role;
	}
	
	public SignUpForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
