package com.example.demo.dto;

public class UserStatInfo {
	
	private String name; 
	private float montantTotalPourTousLesCommandes;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public float getMontantTotalPourTousLesCommandes() {
		return montantTotalPourTousLesCommandes;
	}
	
	public void setMontantTotalPourTousLesCommandes(float montantTotalPourTousLesCommandes) {
		this.montantTotalPourTousLesCommandes = montantTotalPourTousLesCommandes;
	}
	
	
	public UserStatInfo(String name, float montantTotalPourTousLesCommandes) {
		super();
		this.name = name;
		this.montantTotalPourTousLesCommandes = montantTotalPourTousLesCommandes;
	}
	
	public UserStatInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
   
}
