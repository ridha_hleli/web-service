package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Objectif;
import com.example.demo.Service.ObjectifService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Objectif")
public class ObjectifController {
	
	private final ObjectifService service;


    public ObjectifController(ObjectifService service) {
		super();
		this.service = service;
	}

	@GetMapping("/objectifs")
  public ResponseEntity<List<Objectif>> getAllObjectifs() {
        List<Objectif> obj = service.findAllObjectif();
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
    @PostMapping("/add")
    public ResponseEntity<Objectif> addObjectif(@RequestBody Objectif obj) {
    	 
    	Objectif newObj = service.addObjectif(obj);
        return new ResponseEntity<>(newObj, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Objectif> updateObjectif(@RequestBody Objectif obj) {
    	Objectif updateObj = service.updateObjectif(obj);
        return new ResponseEntity<>(updateObj, HttpStatus.OK);
    }
    
    @GetMapping("/find/{idObj}")
    public ResponseEntity<Objectif> getObjectifByIdObj (@PathVariable("idObj") Integer idObj) {
    	Objectif obj = service.findObjectifByIdObjectif(idObj);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
  

}
