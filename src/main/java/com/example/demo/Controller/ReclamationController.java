package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Réclamation;
import com.example.demo.Service.ReclamationService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Reclamation")
public class ReclamationController {

	private final ReclamationService service;

	public ReclamationController(ReclamationService service) {
		super();
		this.service = service;
	}

    @GetMapping("/reclamations")
  public ResponseEntity<List<Réclamation>> getAllReclamations() {
        List<Réclamation> rec = service.findAllRéclamation();
        return new ResponseEntity<>(rec, HttpStatus.OK);
    }
    
    @GetMapping("/reclamations/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idRec}")
    public ResponseEntity<Réclamation> getReclamationByIdRec (@PathVariable("idRec") Integer idRec) {
    	Réclamation rec = service.findRéclamationByIdRéclamation(idRec);
        return new ResponseEntity<>(rec, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Réclamation> addReclamation(@RequestBody Réclamation rec) {
    	 
    	Réclamation newrec = service.addReclamtion(rec);
        return new ResponseEntity<>(newrec, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Réclamation> updateReclamation(@RequestBody Réclamation rec) {
    	Réclamation updateRec = service.updateRéclamation(rec);
        return new ResponseEntity<>(updateRec, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idRec}")
    public ResponseEntity<?> deleteReclamation(@PathVariable("idRec") Integer idRec) {
        service.deleteReclamation(idRec);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}