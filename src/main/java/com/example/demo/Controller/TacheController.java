package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Tache;
import com.example.demo.Model.Visite;
import com.example.demo.Service.TacheService;
import com.example.demo.Service.VisiteService;


@RestController
@CrossOrigin("*")
@RequestMapping("/Tache")
public class TacheController {

	private final TacheService service;

	public TacheController(TacheService service) {
		super();
		this.service = service;
	}

    @GetMapping("/taches")
	public ResponseEntity<List<Tache>> getAllTaches() {
        List<Tache> tache = service.findAllTaches();
        return new ResponseEntity<>(tache, HttpStatus.OK);
    }
    
    @GetMapping("/taches/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idTache}")
    public ResponseEntity<Tache> getTacheByIdTache (@PathVariable("idRec") Long idTache) {
    	Tache tache = service.findTacheById(idTache);
        return new ResponseEntity<>(tache, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Tache> addTache(@RequestBody Tache tache) {
    	 
    	Tache newTache = service.addTache(tache);
        return new ResponseEntity<>(newTache, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Tache> updateTache(@RequestBody Tache tache) {
    	Tache updateTache = service.updateTache(tache);
        return new ResponseEntity<>(updateTache, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idTache}")
    public ResponseEntity<?> deleteTache(@PathVariable("idTache") Long idTache) {
        service.deleteTache(idTache);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
