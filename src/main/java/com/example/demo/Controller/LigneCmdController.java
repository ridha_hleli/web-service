package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.ligne_commande;
import com.example.demo.Service.LigneCmdService;
import com.example.demo.dto.UpdateLigneCommandeByQteRequest;

@RestController
@CrossOrigin("*")
@RequestMapping("/lc")
public class LigneCmdController {

	private final LigneCmdService service;

	public LigneCmdController(LigneCmdService service) {
		super();
		this.service = service;
	}

    @GetMapping("/lcs")
    public ResponseEntity<List<ligne_commande>> getAllligne_commandes() {
        List<ligne_commande> lcs = service.findAllligne_commandes();
        return new ResponseEntity<>(lcs, HttpStatus.OK);
    }
    @GetMapping("/ligne_commandes/count")
    public Long count() {
        return service.count();
    }
    

    @GetMapping("/find/{idligneCmd}")
    public ResponseEntity<ligne_commande> getligne_commandeById (@PathVariable("idligne_commande") Long idligneCmd) {
    	ligne_commande lc = service.findligne_commandeByIdligneCmd(idligneCmd);
        return new ResponseEntity<>(lc, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<ligne_commande> addligne_commande(@RequestBody ligne_commande lc) {
    	 
    	ligne_commande newlc = service.addligne_commande(lc);
        return new ResponseEntity<>(newlc, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<ligne_commande> updateligne_commande(@RequestBody ligne_commande lc) {
    	ligne_commande updatelc = service.updateligne_commande(lc);
        return new ResponseEntity<>(updatelc, HttpStatus.OK);
    }
    
    @PutMapping("/updateQte")
    public ResponseEntity<ligne_commande> updateByQte(@RequestBody UpdateLigneCommandeByQteRequest updateLigneCommandeByQteRequest) { 
    	
        return new ResponseEntity<>(service.updateLigneCommandeQte(updateLigneCommandeByQteRequest), HttpStatus.OK);
    }
    
    @DeleteMapping("/delete/{idligneCmd}")
    public ResponseEntity<?> deleteligne_commande(@PathVariable("idligneCmd") Long idligneCmd) {
        service.deleteligne_commande(idligneCmd);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
