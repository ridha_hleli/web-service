package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.TypeVisite;
import com.example.demo.Service.TypeVisiteService;


@RestController
@CrossOrigin("*")
@RequestMapping("/TypeVisite")
public class TypeVisiteController {
	private final TypeVisiteService service;

	public TypeVisiteController(TypeVisiteService service) {
		super();
		this.service = service;
	}

    @GetMapping("/typesVisite")
  public ResponseEntity<List<TypeVisite>> getAllTypeVisite() {
        List<TypeVisite> typeV = service.findAllTypeVisite();
        return new ResponseEntity<>(typeV, HttpStatus.OK);
    }
    
    @GetMapping("/typesVisite/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idTypeV}")
    public ResponseEntity<TypeVisite> getTypeVisiteByIdTypeV (@PathVariable("idTypeV") Integer idTypeV) {
    	TypeVisite typeV = service.findTypeVisiteById(idTypeV);
        return new ResponseEntity<>(typeV, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<TypeVisite> addTypeVisite(@RequestBody TypeVisite typeV) {
    	 
    	TypeVisite newTypeV = service.addTypeVisite(typeV);
        return new ResponseEntity<>(newTypeV, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<TypeVisite> updateTypeVisite(@RequestBody TypeVisite typeV) {
    	TypeVisite updateTypeV = service.updateTypeVisite(typeV);
        return new ResponseEntity<>(updateTypeV, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idTypeV}")
    public ResponseEntity<?> deleteTypeVisite(@PathVariable("idTypeV") Integer idTypeV) {
        service.deleteTypeVisite(idTypeV);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}