package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.TypeReclamation;
import com.example.demo.Service.TypeReclamationService;

@RestController
@CrossOrigin("*")
@RequestMapping("/TypeReclamation")
public class TypeReclamationController {
	private final TypeReclamationService service;

	public TypeReclamationController(TypeReclamationService service) {
		super();
		this.service = service;
	}

    @GetMapping("/Typesreclamations")
  public ResponseEntity<List<TypeReclamation>> getAllTypeReclamations() {
        List<TypeReclamation> Typerec = service.findAllTypeReclamation();
        return new ResponseEntity<>(Typerec, HttpStatus.OK);
    }
    
    @GetMapping("/Typesreclamations/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idTypeRec}")
    public ResponseEntity<TypeReclamation> getTypeReclamationByIdRec (@PathVariable("idTypeRec") Integer idTypeRec) {
    	TypeReclamation Typerec = service.findTypeReclamationById(idTypeRec);
        return new ResponseEntity<>(Typerec, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<TypeReclamation> addTypeReclamation(@RequestBody TypeReclamation Typerec) {
    	 
    	TypeReclamation newTyperec = service.addTypeRec(Typerec);
        return new ResponseEntity<>(newTyperec, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<TypeReclamation> updateTypeReclamation(@RequestBody TypeReclamation Typerec) {
    	TypeReclamation updateTypeRec = service.updateTypeReclamation(Typerec);
        return new ResponseEntity<>(updateTypeRec, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idTypeRec}")
    public ResponseEntity<?> deleteTypeReclamation(@PathVariable("idTypeRec") Integer idTypeRec) {
        service.deleteTypeRec(idTypeRec);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}