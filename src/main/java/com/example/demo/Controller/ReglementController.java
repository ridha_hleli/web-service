package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Réglement;
import com.example.demo.Service.ReglementService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Reglement")
public class ReglementController {

	private final ReglementService service;

	public ReglementController(ReglementService service) {
		super();
		this.service = service;
	}

    @GetMapping("/reglements")
  public ResponseEntity<List<Réglement>> getAllreglements() {
        List<Réglement> reg = service.findAllReglement();
        return new ResponseEntity<>(reg, HttpStatus.OK);
    }
    
    @GetMapping("/reglements/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idreg}")
    public ResponseEntity<Réglement> getReglementByIdreg (@PathVariable("idreg") Integer idreg) {
    	Réglement reg = service.findByIdReglement(idreg);
        return new ResponseEntity<>(reg, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Réglement> addReglement(@RequestBody Réglement reg) {
    	 
    	Réglement newreg = service.addReglement(reg);
        return new ResponseEntity<>(newreg, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Réglement> updateReglement(@RequestBody Réglement reg) {
    	Réglement updatereg = service.updateReglement(reg);
        return new ResponseEntity<>(updatereg, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idreg}")
    public ResponseEntity<?> deleteReglement(@PathVariable("idreg") Integer idreg) {
        service.deleteReglement(idreg);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}