package com.example.demo.Controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Facture;
import com.example.demo.Service.FactureService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Facture")
public class FactureController {
	private final FactureService service;

	public FactureController(FactureService service) {
		super();
		this.service = service;
	}

    @GetMapping("/factures")
    public ResponseEntity<List<Facture>> getAllFactures() {
        List<Facture> facts = service.findAllFactures();
        return new ResponseEntity<>(facts, HttpStatus.OK);
    }
    @GetMapping("/factures/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idFacture}")
    public ResponseEntity<Facture> getFactureById (@PathVariable("idFacture") Integer idFacture) {
    	Facture fact = service.findFactureByIdfacture(idFacture);
        return new ResponseEntity<>(fact, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Facture> addFacture(@RequestBody Facture fact) {
    	 
    	Facture newfact = service.addFacture(fact);
        return new ResponseEntity<>(newfact, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Facture> updateFacture(@RequestBody Facture fact) {
    	Facture updateFact = service.updateFacture(fact);
        return new ResponseEntity<>(updateFact, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idFacture}")
    public ResponseEntity<?> deleteFacture(@PathVariable("idFacture") Integer idFacture) {
        service.deleteFacture(idFacture);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}