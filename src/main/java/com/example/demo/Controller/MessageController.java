package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Message;
import com.example.demo.Service.MessageService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Message")
public class MessageController {
	

	private final MessageService service;


    public MessageController(MessageService service) {
		super();
		this.service = service;
	}

	@GetMapping("/messages")
  public ResponseEntity<List<Message>> getAllMessages() {
        List<Message> mesg = service.findAllMessage();
        return new ResponseEntity<>(mesg, HttpStatus.OK);
    }
    
    @GetMapping("/messages/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idMesg}")
    public ResponseEntity<Message> getMessageByIdMesg (@PathVariable("idMesg") Integer idMesg) {
    	Message mesg = service.findMessageByIdMessage(idMesg);
        return new ResponseEntity<>(mesg, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Message> addMessage(@RequestBody Message mesg) {
    	 
    	Message newmesg = service.addMessage(mesg);
        return new ResponseEntity<>(newmesg, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Message> updateMessage(@RequestBody Message mesg) {
    	Message updateMesg = service.updateMessage(mesg);
        return new ResponseEntity<>(updateMesg, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idMesg}")
    public ResponseEntity<?> deleteMessage(@PathVariable("idMesg") Integer idMesg) {
        service.deleteMessage(idMesg);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

