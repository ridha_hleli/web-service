package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Visite;
import com.example.demo.Service.VisiteService;

@RestController
@CrossOrigin("*")
@RequestMapping("/Visite")
public class VisiteController {

	private final VisiteService service;

	public VisiteController(VisiteService service) {
		super();
		this.service = service;
	}

    @GetMapping("/visites")
	public ResponseEntity<List<Visite>> getAllVisites() {
        List<Visite> visite = service.findAllVisites();
        return new ResponseEntity<>(visite, HttpStatus.OK);
    }
    
    @GetMapping("/visites/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idVisite}")
    public ResponseEntity<Visite> getVisiteByIdVisite (@PathVariable("idRec") Long idVisite) {
    	Visite visite = service.findVisiteById(idVisite);
        return new ResponseEntity<>(visite, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<Visite> addVisite(@RequestBody Visite visite) {
    	 
    	Visite newvisite = service.addVisite(visite);
        return new ResponseEntity<>(newvisite, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Visite> updateVisite(@RequestBody Visite visite) {
    	Visite updateVisite = service.updateVisite(visite);
        return new ResponseEntity<>(updateVisite, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idVisite}")
    public ResponseEntity<?> deleteVisite(@PathVariable("idVisite") Long idVisite) {
        service.deleteVisite(idVisite);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}