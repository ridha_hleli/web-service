package com.example.demo.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.TypeReglement;
import com.example.demo.Service.TypeReglementService;

@RestController
@CrossOrigin("*")
@RequestMapping("/TypeReglement")
public class TypeReglementController {
	private final TypeReglementService service;

	public TypeReglementController(TypeReglementService service) {
		super();
		this.service = service;
	}

    @GetMapping("/TypeReglements")
  public ResponseEntity<List<TypeReglement>> getAllTypeReglements() {
        List<TypeReglement> typeReg = service.findAllTypeReg();
        return new ResponseEntity<>(typeReg, HttpStatus.OK);
    }
    
    @GetMapping("/TypeReglements/count")
    public Long count() {

        return service.count();
    }
    

    @GetMapping("/find/{idTypeReg}")
    public ResponseEntity<TypeReglement> getTypeReglementByIdTypeReg (@PathVariable("idTypeReg") Integer idTypeReg) {
    	TypeReglement typeReg = service.findTypeRegByIdTypeReg(idTypeReg);
        return new ResponseEntity<>(typeReg, HttpStatus.OK);
    }
    
    @PostMapping("/add")
    public ResponseEntity<TypeReglement> addTypeReglement(@RequestBody TypeReglement typeReg) {
    	 
    	TypeReglement newTypeReg = service.addTypeReg(typeReg);
        return new ResponseEntity<>(newTypeReg, HttpStatus.CREATED);
    }
    
    @PutMapping("/update")
    public ResponseEntity<TypeReglement> updateTypeReglement(@RequestBody TypeReglement typeReg) {
    	TypeReglement updateTReg = service.updateTypeReg(typeReg);
        return new ResponseEntity<>(updateTReg, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idTypeReg}")
    public ResponseEntity<?> deleteTypeReglement(@PathVariable("idTypeReg") Integer idTypeReg) {
        service.deleteTypeReg(idTypeReg);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}