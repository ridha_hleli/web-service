

package com.example.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Categories;
import com.example.demo.Repository.categoriesRepository;
import com.example.demo.Service.CategorieService;
import java.util.Optional;

@RestController
@RequestMapping("/categorie")
@CrossOrigin(origins ="*")  

public class CategorieController {
	private final CategorieService service;
	@Autowired
	public categoriesRepository repo;

	public CategorieController(CategorieService service) {
		super();
		this.service = service;
	}

	@GetMapping("/categories")
	public ResponseEntity<List<Categories>> getAllCategories() {
		List<Categories> categorie = service.findAllCategories();
		return new ResponseEntity<>(categorie, HttpStatus.OK);
	}

	@GetMapping("/count")
	public Long count() {

		return service.count();
	}

	@GetMapping("/{idCategorie}")
	public ResponseEntity<Categories> getcategorieById(@PathVariable("idCategorie") Long idCategorie) {
		Categories categorie = service.findCategorieByIdCategorie(idCategorie);
		return new ResponseEntity<>(categorie, HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<Categories> addcategorie(@RequestBody Categories cat) {

		Categories newcat = service.addCategorie(cat);
		return new ResponseEntity<>(newcat, HttpStatus.CREATED);
	}
	 
	  @PutMapping("/update/{idCategorie}")
	 public ResponseEntity<Categories> modifierCategorie(@PathVariable("idCategorie") Long idCategorie,@RequestBody Categories categorie) {
		   Optional<Categories> cat =repo.findById(idCategorie);
		  
		   if(cat.isPresent()) {
			  System.out.println(cat.toString());
		  Categories _cat=cat.get();
		   _cat.setNomCategorie(categorie.getNomCategorie());
		   _cat.setDescription(categorie.getDescription());
		
			return new ResponseEntity<>(repo.save(_cat), HttpStatus.OK);
			}
		  else{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		  }
		  }

	/*
	  @PutMapping("/update/{idCategorie}")
			public ResponseEntity<Categories> updateCategoriee(@PathVariable("idCategorie") Long idCategorie,@RequestBody Categories categorie){
		  
		  //service.update(idCategorie, categorie);
		  java.util.Optional<Categories> cat =repo.findCategoriesByIdCategorie(idCategorie);
		  System.out.println(cat);
		  if(cat.isPresent()) {
			  Categories catupdate=cat.get();
			  
			  catupdate.setDescription(categorie.getDescription());
			  catupdate.setNomCategorie(categorie.getNomCategorie());
		  
				return new ResponseEntity<>(repo.save(catupdate), HttpStatus.OK);
			}
		  else{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		  }
		  }
	  
	  */
	@PutMapping("/update")
	public ResponseEntity<Categories> updatecategorie(@RequestBody Categories cat) {
		Categories updatecat = service.updateCategorie(cat);
		return new ResponseEntity<>(updatecat, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{idCategorie}")
	public ResponseEntity<?> deletecategorie(@PathVariable("idCategorie") Long idCategorie) {
		service.deleteCategorie(idCategorie);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
